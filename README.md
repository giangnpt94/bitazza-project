This project is a demo project of websocket reactjs connect to nodejs.

## Available Scripts
This project includes 2 parts: front-end and back-end.
The front-end project locates at the root of the project. The back-end project locates at the ./backend folder.
In the project directory, you can run:

### Back-end 

#### install (Downloading and installing packages locally)
npm install  

#### Run 
npm start

### Front-end 

#### install (Downloading and installing packages locally)
npm install  

#### Run 
npm start

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.
