import './App.css';
import react, { Suspense } from 'react';
import { BrowserRouter } from 'react-router-dom';
import { Redirect, Route, Switch } from 'react-router-dom';
import NotFound from './components/NotFound';
import Header from './components/Header';
import { PrivateRoute } from 'app/PrivateRoute';
import Login from 'features/User/pages/Login';
import Instrument from 'features/Instrument';

// const Login = react.lazy(() => import('./features/User/pages/Login'));
// const Instrument = react.lazy(() => import('./features/Instrument'));

function App() {

  return (
    <div className="photo-app">
      <Suspense fallback={<div>Loading ...</div>}>
        <BrowserRouter>

          <Header />

          <Switch>
            <Redirect exact from="/" to="/instruments"></Redirect>

            <Route path="/login" component={Login} ></Route>
            <PrivateRoute path="/instruments" component={Instrument} />


            <Route component={NotFound} ></Route>
          </Switch>
        </BrowserRouter>
      </Suspense>
    </div>
  );
}

export default App;
