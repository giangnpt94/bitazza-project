import { WebSocketContext } from 'app/websocket/WebSocket';
import { clearState } from 'features/User/userSlice';
import { userSelector } from 'features/User/userSlice';
import React, { useContext, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { NavLink } from 'react-router-dom';
import { Button, Col, Container, Row } from 'reactstrap';
import './Header.scss';
import { useHistory } from 'react-router';

Header.propTypes = {};

function Header() {
  const { data } = useSelector(userSelector);
  const ws = useContext(WebSocketContext);
  const history = useHistory();

  const handleLogout = () => {
    ws.sendMessage("LogOut", {});
    new Promise(resolve => {
      setTimeout(() => {
        if (!localStorage.getItem('token')) {
          history.push('/');
        }
        resolve(true);
      }, 500);
    });
  }

  return (
    <header className="fixed-top header bg-white">
      <Container>
        <Row className="d-flex justify-content-between">
          <Col xs="auto">
            <NavLink
              exact
              className="header__link header__title"
              to="/"
            >
              Bitazza
            </NavLink>
          </Col>


          <Col xs="auto">
            {localStorage.getItem('token') ? (
              <Button className="bg-white text-black" onClick={handleLogout}>
                Log out
              </Button>
            ) : (
              <NavLink
                exact
                className="header__link"
                to="/login"
                activeClassName="header__link--active"
              >
                Sign In
              </NavLink>
            )}
          </Col>
        </Row>
      </Container>
    </header>
  );
}

export default Header;