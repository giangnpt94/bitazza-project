import React from 'react';
import { Toast, ToastBody, ToastHeader } from 'reactstrap';

MyToast.propTypes = {};

function MyToast(props) {
  const { show, toggle, errorMessage } = props;

  return (
    <div className="position-absolute bottom-0 end-0 p-3 my-2 rounded b-0 r-0">
      <Toast isOpen={show}>
        <ToastHeader toggle={toggle} className="d-flex justify-content-between">
          Login
        </ToastHeader>
        <ToastBody>
          {errorMessage}
        </ToastBody>
      </Toast>
    </div>
  );
}

export default MyToast;