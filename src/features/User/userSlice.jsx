import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';

export const loginReply = createAsyncThunk(
    'users/loginReply',
    async (loginReply, thunkAPI) => {
        console.log('loginReply', loginReply);

        if (loginReply.authenticated) {
            localStorage.setItem('token', loginReply.twoFAToken);
            return loginReply;
        }

        return thunkAPI.rejectWithValue(loginReply);
    }
);

export const logoutReply = createAsyncThunk(
    'users/logoutReply',
    async (logoutReply, thunkAPI) => {
        console.log('logoutReply', logoutReply);

        if (logoutReply.result) {
            localStorage.removeItem('token');
            return loginReply;
        }

        return thunkAPI.rejectWithValue(logoutReply);
    }
);

const user = createSlice({
    name: "user",
    initialState: {
        data: {},
        isFetching: false,
        isSuccess: false,
        isError: false,
        errorMessage: '',
    },
    reducers: {
        clearState: (state) => {
            state.isError = false;
            state.isSuccess = false;
            state.isFetching = false;

            return state;
        },
    },
    extraReducers: {
        [logoutReply.fulfilled]: (state, { payload }) => {
            state.data = {};
            state.isFetching = false;
            state.isSuccess = true;
            return state;
        },
        [loginReply.pending]: (state) => {
            state.isFetching = true;
        },
        [loginReply.fulfilled]: (state, { payload }) => {
            state.data = payload;
            state.isFetching = false;
            state.isSuccess = true;
            return state;
        },
        [loginReply.rejected]: (state, { payload }) => {
            console.log('payload', payload);
            state.isFetching = false;
            state.isError = true;
            state.errorMessage = payload.errormsg;
        }
    },
});

const { reducer, actions } = user;
export const { clearState } = actions;
export const userSelector = (state) => state.user;
export default reducer;