import InputField from 'custom-fields/InputField';
import { FastField, Form, Formik } from 'formik';
import PropTypes from 'prop-types';
import React from 'react';
import { Button, FormGroup, Spinner } from 'reactstrap';
import * as Yup from 'yup';

LoginForm.propTypes = {
    onSubmit: PropTypes.func,
};

LoginForm.defaultProps = {
    onSubmit: null,
}

function LoginForm(props) {
    const { initialValues } = props;

    const validationSchema = Yup.object().shape({
        username: Yup.string().required('This field is required.'),
        password: Yup.string().required('This field is required.')
    });

    // npm i --save react-select
    return (
        <Formik
            initialValues={initialValues}
            validationSchema={validationSchema}
            onSubmit={props.onSubmit}
        >
            {formikProps => {
                const { isSubmitting } = formikProps;

                return (
                    <Form>
                        <FastField
                            name="username"
                            component={InputField}

                            label="Username"
                        />

                        <FastField
                            name="password"
                            component={InputField}
                            type="password"
                            label="Password"
                        />

                        <FormGroup className="mt-2">
                            <Button type="submit" color={'success'} disabled={isSubmitting}>
                                {isSubmitting && <span><Spinner size="sm" color="white">{''}</Spinner>{' '}</span>}
                                Login
                            </Button>
                        </FormGroup>
                    </Form>
                );
            }}
        </Formik>
    );
}

export default LoginForm;