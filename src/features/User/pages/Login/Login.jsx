import React, { useEffect, useContext, useState } from 'react';
import { WebSocketContext } from 'app/websocket/WebSocket';
import { useDispatch, useSelector } from 'react-redux';
import { userSelector, clearState } from 'features/User/userSlice';
import LoginForm from 'features/User/components/LoginForm';
import './login.scss';
import { useHistory } from 'react-router';
import MyToast from 'components/MyToast/MyToast';

Login.propTypes = {};

function Login() {
    const initialValues = {
        username: '',
        password: ''
    };

    const history = useHistory();
    const dispatch = useDispatch();
    const [show, setShow] = useState(false);
    const { isError, isSuccess, data, errorMessage } = useSelector(userSelector);
    const ws = useContext(WebSocketContext);

    const handleSubmit = (values) => {
        return new Promise(resolve => {
            console.log('Form submit: ', values);

            setTimeout(() => {
                ws.sendMessage("AuthenticateUser", values);
                resolve(true);
            }, 100);
        });
    }

    const toggle = () => {
        setShow(true);
        new Promise(resolve => {
            setTimeout(() => {
                setShow(false);
                resolve(true);
            }, 3000);
        });
    };

    useEffect(() => {
        if (isError) {
            toggle();
            // dispatch(clearState());
        }

        if (isSuccess) {
            if (data.authenticated) {
                // dispatch(clearState());
                history.push('/');
            }
            toggle();
            // dispatch(clearState());
        }
    }, [isError, isSuccess, data.authenticated, history, dispatch]);

    return (
        <div className="min-vh-100 bg-white d-flex align-items-center justify-content-center">
            <MyToast show={show} toggle={toggle} errorMessage={errorMessage} />
            <div className="border shadow p-4 user-login__form bg-white rounded flex-fill">
                <LoginForm
                    initialValues={initialValues}
                    onSubmit={handleSubmit}
                />
            </div>
        </div>
    );
}

export default Login;