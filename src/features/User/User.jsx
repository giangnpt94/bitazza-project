import React from 'react';
import { Route, Switch, useRouteMatch } from 'react-router-dom';
import NotFound from 'components/NotFound';
import Login from './pages/Login';

User.propTypes = {};

function User() {
    const match = useRouteMatch();
    console.log({ match });

    return (
        <Switch>
            <Route exact path={match.url} component={Login}></Route>
            <Route component={NotFound}></Route>
        </Switch>
    );
}

export default User;