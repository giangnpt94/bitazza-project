import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';
import instrumentApi from 'api/instrumentApi';

export const getTop = createAsyncThunk(
    'instruments/getTop',
    async ({ topType, startDate, endDate }, thunkAPI) => {
        console.log('getTopIntruments', { topType, startDate, endDate });

        let response = [];
        if (1 === topType) {
            response = await instrumentApi.getTopGainers(startDate, endDate);
            return response;
        }
        if (2 === topType) {
            response = await instrumentApi.getTopLosers(startDate, endDate);
            return response;
        }

        return thunkAPI.rejectWithValue(response);
    }
);

const instrument = createSlice({
    name: "instrument",
    initialState: {
        data: [],
        isFetching: false,
        isSuccess: false,
        isError: false,
        errorMessage: '',
    },
    reducers: {
        clearState: (state) => {
            state.isError = false;
            state.isSuccess = false;
            state.isFetching = false;

            return state;
        },
    },
    extraReducers: {
        [getTop.pending]: (state) => {
            state.isFetching = true;
        },
        [getTop.fulfilled]: (state, { payload }) => {
            state.data = payload;
            state.isFetching = false;
            state.isSuccess = true;
            return state;
        },
        [getTop.rejected]: (state, { payload }) => {
            console.log('payload', payload);
            state.isFetching = false;
            state.isError = true;

            try {
                state.errorMessage = payload.errormsg;
            } catch (e) {
                state.errorMessage = "Get top instruments timeout";
            }
        }
    },
});

const { reducer, actions } = instrument;
export const { clearState } = actions;
export const instrumentSelector = (state) => state.instrument;
export default reducer;