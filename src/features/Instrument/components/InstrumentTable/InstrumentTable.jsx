import React from 'react';
import PropTypes from 'prop-types';
import { Table } from 'reactstrap';

InstrumentTable.propTypes = {
};

InstrumentTable.defaultProps = {
    data: [
        [
            {
                "OMSId": 1,
                "InstrumentId": 5,
                "Symbol": "USDTTHB",
                "Product1": 7,
                "Product1Symbol": "USDT",
                "Product2": 3,
                "Product2Symbol": "THB",
                "InstrumentType": "Standard",
                "VenueInstrumentId": 6,
                "VenueId": 0,
                "SortIndex": 0,
                "SessionStatus": "Running",
                "PreviousSessionStatus": "Running",
                "SessionStatusDateTime": "2021-02-03T22:30:14.583Z",
                "SelfTradePrevention": true,
                "QuantityIncrement": 0.01,
                "PriceIncrement": 0.9956279693374916,
                "MinimumQuantity": 0.01,
                "MinimumPrice": 0.001,
                "VenueSymbol": "USDTTHB",
                "IsDisable": false,
                "MasterDataId": 0,
                "PriceCollarThreshold": 0,
                "PriceCollarPercent": 5,
                "PriceCollarEnabled": false,
                "PriceFloorLimit": 0,
                "PriceFloorLimitEnabled": false,
                "PriceCeilingLimit": 0,
                "PriceCeilingLimitEnabled": false,
                "CreateWithMarketRunning": true,
                "AllowOnlyMarketMakerCounterParty": false,
                "PriceCollarIndexDifference": 15,
                "PriceCollarConvertToOtcEnabled": false,
                "PriceCollarConvertToOtcClientUserId": 0,
                "PriceCollarConvertToOtcAccountId": 0,
                "PriceCollarConvertToOtcThreshold": 0,
                "OtcConvertSizeThreshold": 0,
                "OtcConvertSizeEnabled": false,
                "OtcTradesPublic": false,
                "PriceTier": 0
            },
            {
                "OMSId": 1,
                "InstrumentId": 6,
                "Symbol": "BTZTHB",
                "Product1": 8,
                "Product1Symbol": "BTZ",
                "Product2": 3,
                "Product2Symbol": "THB",
                "InstrumentType": "Standard",
                "VenueInstrumentId": 7,
                "VenueId": 0,
                "SortIndex": 0,
                "SessionStatus": "Stopped",
                "PreviousSessionStatus": "Running",
                "SessionStatusDateTime": "2021-07-17T09:17:08.007Z",
                "SelfTradePrevention": true,
                "QuantityIncrement": 1e-8,
                "PriceIncrement": 1,
                "MinimumQuantity": 1e-8,
                "MinimumPrice": 0.01,
                "VenueSymbol": "BTZTHB",
                "IsDisable": false,
                "MasterDataId": 0,
                "PriceCollarThreshold": 0,
                "PriceCollarPercent": 0,
                "PriceCollarEnabled": false,
                "PriceFloorLimit": 0,
                "PriceFloorLimitEnabled": false,
                "PriceCeilingLimit": 0,
                "PriceCeilingLimitEnabled": false,
                "CreateWithMarketRunning": false,
                "AllowOnlyMarketMakerCounterParty": false,
                "PriceCollarIndexDifference": 0,
                "PriceCollarConvertToOtcEnabled": false,
                "PriceCollarConvertToOtcClientUserId": 0,
                "PriceCollarConvertToOtcAccountId": 0,
                "PriceCollarConvertToOtcThreshold": 0,
                "OtcConvertSizeThreshold": 0,
                "OtcConvertSizeEnabled": false,
                "OtcTradesPublic": true,
                "PriceTier": 0
            },
            {
                "OMSId": 1,
                "InstrumentId": 12,
                "Symbol": "QBTCTHB",
                "Product1": 1,
                "Product1Symbol": "BTC",
                "Product2": 3,
                "Product2Symbol": "THB",
                "InstrumentType": "Standard",
                "VenueInstrumentId": 12,
                "VenueId": 1,
                "SortIndex": 0,
                "SessionStatus": "Running",
                "PreviousSessionStatus": "Running",
                "SessionStatusDateTime": "2021-02-03T22:30:15.130Z",
                "SelfTradePrevention": true,
                "QuantityIncrement": 1e-8,
                "PriceIncrement": 1,
                "MinimumQuantity": 1e-8,
                "MinimumPrice": 1e-8,
                "VenueSymbol": "QBTCTHB",
                "IsDisable": false,
                "MasterDataId": 0,
                "PriceCollarThreshold": 0,
                "PriceCollarPercent": 0,
                "PriceCollarEnabled": false,
                "PriceFloorLimit": 0,
                "PriceFloorLimitEnabled": false,
                "PriceCeilingLimit": 0,
                "PriceCeilingLimitEnabled": false,
                "CreateWithMarketRunning": true,
                "AllowOnlyMarketMakerCounterParty": false,
                "PriceCollarIndexDifference": 0,
                "PriceCollarConvertToOtcEnabled": false,
                "PriceCollarConvertToOtcClientUserId": 0,
                "PriceCollarConvertToOtcAccountId": 0,
                "PriceCollarConvertToOtcThreshold": 0,
                "OtcConvertSizeThreshold": 0,
                "OtcConvertSizeEnabled": false,
                "OtcTradesPublic": false,
                "PriceTier": 0
            },
            {
                "OMSId": 1,
                "InstrumentId": 16,
                "Symbol": "QUSDTTHB",
                "Product1": 7,
                "Product1Symbol": "USDT",
                "Product2": 3,
                "Product2Symbol": "THB",
                "InstrumentType": "Standard",
                "VenueInstrumentId": 16,
                "VenueId": 1,
                "SortIndex": 0,
                "SessionStatus": "Running",
                "PreviousSessionStatus": "Running",
                "SessionStatusDateTime": "2021-02-03T22:30:15.427Z",
                "SelfTradePrevention": true,
                "QuantityIncrement": 1e-8,
                "PriceIncrement": 1,
                "MinimumQuantity": 1e-8,
                "MinimumPrice": 1e-8,
                "VenueSymbol": "QUSDTTHB",
                "IsDisable": false,
                "MasterDataId": 0,
                "PriceCollarThreshold": 0,
                "PriceCollarPercent": 0,
                "PriceCollarEnabled": false,
                "PriceFloorLimit": 0,
                "PriceFloorLimitEnabled": false,
                "PriceCeilingLimit": 0,
                "PriceCeilingLimitEnabled": false,
                "CreateWithMarketRunning": true,
                "AllowOnlyMarketMakerCounterParty": false,
                "PriceCollarIndexDifference": 0,
                "PriceCollarConvertToOtcEnabled": false,
                "PriceCollarConvertToOtcClientUserId": 0,
                "PriceCollarConvertToOtcAccountId": 0,
                "PriceCollarConvertToOtcThreshold": 0,
                "OtcConvertSizeThreshold": 0,
                "OtcConvertSizeEnabled": false,
                "OtcTradesPublic": false,
                "PriceTier": 0
            },
            {
                "OMSId": 1,
                "InstrumentId": 19,
                "Symbol": "COMPUSDT",
                "Product1": 9,
                "Product1Symbol": "COMP",
                "Product2": 7,
                "Product2Symbol": "USDT",
                "InstrumentType": "Standard",
                "VenueInstrumentId": 19,
                "VenueId": 1,
                "SortIndex": 0,
                "SessionStatus": "Running",
                "PreviousSessionStatus": "Running",
                "SessionStatusDateTime": "2021-02-03T22:30:15.677Z",
                "SelfTradePrevention": true,
                "QuantityIncrement": 0.00001,
                "PriceIncrement": 1,
                "MinimumQuantity": 0.00001,
                "MinimumPrice": 0.01,
                "VenueSymbol": "COMPUSDT",
                "IsDisable": false,
                "MasterDataId": 0,
                "PriceCollarThreshold": 0,
                "PriceCollarPercent": 0,
                "PriceCollarEnabled": false,
                "PriceFloorLimit": 0,
                "PriceFloorLimitEnabled": false,
                "PriceCeilingLimit": 0,
                "PriceCeilingLimitEnabled": false,
                "CreateWithMarketRunning": true,
                "AllowOnlyMarketMakerCounterParty": false,
                "PriceCollarIndexDifference": 0,
                "PriceCollarConvertToOtcEnabled": false,
                "PriceCollarConvertToOtcClientUserId": 0,
                "PriceCollarConvertToOtcAccountId": 0,
                "PriceCollarConvertToOtcThreshold": 0,
                "OtcConvertSizeThreshold": 0,
                "OtcConvertSizeEnabled": false,
                "OtcTradesPublic": true,
                "PriceTier": 0
            }
        ]
    ]
}

function InstrumentTable(props) {
    const { data } = props;

    const listInstruments = (instrumnets) => instrumnets.map((instrument) =>
        <tr key={instrument.InstrumentId}>
            <th scope="row">{instrument.SortIndex}</th>
            <td>{instrument.Product1Symbol}/{instrument.Product2Symbol}</td>
            <td>{instrument.SessionStatus}</td>
            <td>{instrument.PreviousSessionStatus}</td>
            <td>{instrument.SessionStatusDateTime}</td>
            <td>{instrument.QuantityIncrement}</td>
            <td>{instrument.PriceIncrement}</td>
        </tr>
    );

    return (
        <Table responsive>
            <thead>
                <tr>
                    <th>#</th>
                    <th>Symbol</th>
                    <th>Session Status</th>
                    <th>Previous Session Status</th>
                    <th>session Status Time</th>
                    <th>Quantity Increment</th>
                    <th>Price Increment</th>
                </tr>
            </thead>
            <tbody>
                {listInstruments(data)}
            </tbody>
        </Table>
    );
}

export default InstrumentTable;