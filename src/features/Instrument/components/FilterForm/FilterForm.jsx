import InputField from 'custom-fields/InputField';
import SelectField from 'custom-fields/SelectField';
import { FastField, Form, Formik } from 'formik';
import PropTypes from 'prop-types';
import React, {useState} from 'react';
import * as Yup from 'yup';
import { Button } from 'reactstrap';

FilterForm.propTypes = {
    onSubmit: PropTypes.func,
};

FilterForm.defaultProps = {
    onSubmit: null,
}

const tops = [
    { value: 1, label: "Top gainers" },
    { value: 2, label: "Top losers" }
]

const times = [
    { value: 1, label: "week" },
    { value: 2, label: "month" },
    { value: 3, label: "custom" }
]

function FilterForm(props) {
    const { initialValues } = props;

    let filterField = initialValues;

    const validationSchema = Yup.object().shape({
        topType: Yup.string().required('This field is required.'),
        startDate: Yup.string().required('This field is required.'),
        endDate: Yup.string().required('This field is required.'),
    });

    const getCurrentDate = () => {
        let currentDate = new Date();
        return currentDate.toLocaleDateString("fr-CA")
    }

    const get7DaysAgoDate = () => {
        let sevenDaysAgoDate = new Date();
        sevenDaysAgoDate.setDate(sevenDaysAgoDate.getDate() - 7);
        return sevenDaysAgoDate.toLocaleDateString("fr-CA");
    }

    const get1MonthAgoDate = () => {
        let sevenDaysAgoDate = new Date();
        sevenDaysAgoDate.setMonth(sevenDaysAgoDate.getMonth() - 1);
        return sevenDaysAgoDate.toLocaleDateString("fr-CA");
    }

    return (
        <Formik
            initialValues={filterField}
            validationSchema={validationSchema}
            onSubmit={(filterField) => props.onSubmit(filterField)}
        >
            {formikProps => {
                const { values } = formikProps;

                let isChange = false;
                let newField = values;


                if (values.time !== filterField.time) {
                    newField.endDate = getCurrentDate();
                    newField.startDate = values.time === 1 ? get7DaysAgoDate() : get1MonthAgoDate();
                    filterField = newField;
                 
                }

                if (values.endDate !== filterField.endDate || values.startDate !== filterField.startDate) {
                    if (values.endDate === getCurrentDate() && values.startDate === get7DaysAgoDate()) {
                        newField.time = 1;
                    } else
                    if (values.endDate === getCurrentDate() && values.startDate === get1MonthAgoDate()) {
                        newField.time = 2;
                    } else {
                        newField.time = 3;
                    }
                    
                    filterField = newField;
                }

                if (values.topType !== filterField.topType) {
                   filterField = newField;
                }
  
                return (
                    <Form>
                        <div className="row justify-content-between">
                            <div className="col-2 align-self-start">
                                <FastField
                                    name="topType"
                                    component={SelectField}
                                    options={tops}
                                />
                            </div>
                            <div className="col-7">
                                <div className="row">
                                    <div className="col">
                                        <FastField
                                            name="startDate"
                                            component={InputField}
                                            type="date"
                                        />
                                    </div>
                                    <div className="col">
                                        <FastField
                                            name="endDate"
                                            component={InputField}
                                            type="date"
                                        />
                                    </div>
                                    <div className="col">
                                        <FastField
                                            name="time"
                                            component={SelectField}
                                            options={times}
                                        />
                                    </div>
                                    <div className="col">
                                        <Button type="submit">Search</Button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </Form>
                );
            }}
        </Formik >
    );
}

export default FilterForm;