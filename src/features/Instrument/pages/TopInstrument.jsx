import React, { useLayoutEffect, useRef, useState } from 'react';
import Banner from 'components/Banner';
import Images from 'constants/images';
import FilterForm from 'features/Instrument/components/FilterForm';
import { useDispatch, useSelector } from 'react-redux';
import InstrumentTable from 'features/Instrument/components/InstrumentTable';
import { getTop, instrumentSelector } from 'features/Instrument/instrumentSlice';

TopInstrument.propTypes = {};

function TopInstrument() {
    const dispatch = useDispatch();
    const { data } = useSelector(instrumentSelector);

    const getCurrentDate = () => {
        let currentDate = new Date();
        return currentDate.toLocaleDateString("fr-CA")
    }

    const get7DaysAgoDate = () => {
        let sevenDaysAgoDate = new Date();
        sevenDaysAgoDate.setDate(sevenDaysAgoDate.getDate() - 7);
        return sevenDaysAgoDate.toLocaleDateString("fr-CA");
    }

    const getTimestamp = (strDate) => {
        const dt = new Date(strDate).getTime();
        return dt / 1000;
    }

    const initialValues = {
        topType: 1,
        startDate: get7DaysAgoDate(),
        endDate: getCurrentDate(),
        time: 1
    };

    const handleSubmit = (values) => {
        return new Promise(resolve => {

            let request = {
                topType: values.topType,
                startDate: getTimestamp(values.startDate),
                endDate: getTimestamp(values.endDate)
            }
            console.log('handleSubmit: ', request);
            dispatch(getTop(request));
            resolve(true);
        });
    }

    


    // firstLoadMethod()

    return (
        <div>
            <Banner title="🎉 Your awesome photos 🎉" backgroundUrl={Images.PINK_BG} />
            <div className="container mt-5">
                <div className="row">
                    <div className="col">
                        <FilterForm initialValues={initialValues}
                            onSubmit={handleSubmit} />
                    </div>
                </div>
                <div className="row mt-3">
                    <div className="col">
                        <InstrumentTable data={data}></InstrumentTable>
                    </div>
                </div>
            </div>
        </div >
    );
}

export default TopInstrument;