import axiosClient from "./axiosClient";

const userApi = {
    login: (username, password) => {
        const url = `/user/login`;
        return axiosClient.post(url, { username, password });
    },
}

export default userApi;