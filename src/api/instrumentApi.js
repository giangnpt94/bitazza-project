import axiosClient from "./axiosClient";

const instrumentApi = {
    getTopGainers: (startDate, endDate) => {
        const url = 'http://localhost:8082/api/v1/gainers?fromTime=' + startDate + '&toTime=' + endDate;
        return axiosClient.get(url);
    },
    getTopLosers: (startDate, endDate) => {
        const url = 'http://localhost:8082/api/v1/losers?fromTime=' + startDate + '&toTime=' + endDate;
        return axiosClient.get(url);
    },
}

export default instrumentApi;