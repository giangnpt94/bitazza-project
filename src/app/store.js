import { configureStore } from '@reduxjs/toolkit';
import userReducer from 'features/User/userSlice';
import instrumentReducer from 'features/Instrument/instrumentSlice';

const rootReducer = {
    user: userReducer,
    instrument: instrumentReducer,
}

const store = configureStore({
    reducer: rootReducer,
});

export default store;