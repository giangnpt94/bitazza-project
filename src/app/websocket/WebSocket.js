import React, { createContext } from 'react'
import { useDispatch } from 'react-redux';
import { loginReply, logoutReply } from 'features/User/userSlice';

const WebSocketContext = createContext(null)

export { WebSocketContext }

const WebSocketProvider = ({ children }) => {
    let socket;
    let ws;

    const dispatch = useDispatch();

    const sendMessage = (functionName, requestPayload) => {
        let frame =
        {
            "m": 0,
            "i": 0,
            "n": functionName,
            "o": ""
        };

        frame.o = JSON.stringify(requestPayload);
        console.log(`[websockets] Send mesage: ` + JSON.stringify(frame));
        socket.send(JSON.stringify(frame));
    }

    if (!socket) {
        socket = new WebSocket('ws://localhost:8081');

        socket.addEventListener("open", () => {
            console.log(`[websockets] Connected to ws://localhost:8081`);
        });

        socket.addEventListener("close", () => {
            console.log(`[websockets] Disconnected from ws://localhost:8081`);
            socket = null;
        });

        socket.addEventListener("message", (event) => {
            if (event?.data) {
                let frame = JSON.parse(event.data);
                console.log('socket received: %s', JSON.stringify(frame));

                if (frame.m === 1) // message of type reply
                {

                    if (frame.n === "AuthenticateUser") {
                        let authenReponse = frame.o;
                        dispatch(loginReply(authenReponse));
                    }

                    if (frame.n === "LogOut") {
                        let logoutResponse = frame.o;
                        dispatch(logoutReply(logoutResponse));
                    }
                }
            }
        });

        ws = {
            socket: socket,
            sendMessage
        }
    }

    return (
        <WebSocketContext.Provider value={ws}>
            {children}
        </WebSocketContext.Provider>
    )
}

export default WebSocketProvider;