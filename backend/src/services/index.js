import AuthService from "./auth";
import ApiService from "./api";

module.exports = {
  AuthService,
  ApiService,
};
