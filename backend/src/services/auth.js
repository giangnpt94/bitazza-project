import httpStatus from 'http-status';
// import userService from './user';
// import Token from '../models';
import ApiError  from '../utils/ApiError';
import { tokenTypes } from  '../config/tokens';

/**
 * Login with username and password
 * @param {string} email
 * @param {string} password
 * @returns {Promise<User>}
 */
const loginUserWithEmailAndPassword = async (email, password) => {
  // const user = await userService.getUserByEmail(email);
  // if (!user || !(await user.isPasswordMatch(password))) {
  //   throw new ApiError(httpStatus.UNAUTHORIZED, 'Incorrect email or password');
  // }
  // return user;
};

/**
 * Logout
 * @param {string} accesToken
 * @returns {Promise}
 */
const logout = async (accesToken) => {
  // const accessTokenDoc = await Token.findOne({ token: accesToken, type: tokenTypes.ACCESS, blacklisted: false });
  // if (!accessTokenDoc) {
  //   throw new ApiError(httpStatus.NOT_FOUND, 'Not found');
  // }
  // await refreshTokenDoc.remove();
};

module.exports = {
  loginUserWithEmailAndPassword,
  logout,
};
