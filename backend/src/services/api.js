import ApiError from "../utils/ApiError";
import httpStatus from "http-status";
import bitazzawsService from './bitazzaws';
var dateFormat = require('dateformat');

/**
 * Get top gainers
 * @param {number} fromTime start time by Unix Timestamp
 * @param {number} toTime end tine by Unix Timestamp
 */
const getGainers = async (fromTime, toTime, limit = 5) => {
  return await getTop(fromTime, toTime, true, limit)
};

/**
 * Get top losers
 * @param {number} fromTime start time by Unix Timestamp
 * @param {number} toTime end tine by Unix Timestamp
 */
 const getlLosers = async (fromTime, toTime, limit = 5) => {
  return await getTop(fromTime, toTime, false, limit)
};


const getTop = async (fromTime, toTime, isGainers, limit) => {
  if (!fromTime || !toTime || fromTime > toTime) {
    throw new ApiError(httpStatus.BAD_REQUEST, "Invalid range");
  }

  let instruments = await bitazzawsService.GetInstruments();
  
  let promises = []

  for (let i = 0; i < instruments.length; i++) {
    const pro = new Promise(async (resolve, reject) => {
      let priceIncrement = await getPriceIncrement(fromTime, toTime, instruments[i].InstrumentId)
      instruments[i].PriceIncrement = priceIncrement
      resolve(priceIncrement)
    });
    promises.push(pro);
  }

  await Promise.all(promises);
  instruments = instruments.filter(instrument => instrument.PriceIncrement)
  if (isGainers) {
    instruments.sort((a, b) => b.PriceIncrement - a.PriceIncrement);
  } else {
    instruments.sort((a, b) => a.PriceIncrement - b.PriceIncrement);
  }
  
  return instruments.slice(0, limit);
};


const getPriceIncrement = async (fromTime, toTime, instrumentId) => {

  let fromDatePricePromise = getPrice(fromTime, instrumentId); 
  let toDatePricePromise = getPrice(toTime, instrumentId); 

  let fromDatePrice = await fromDatePricePromise;
  let toDatePrice = await toDatePricePromise;
  
  if (!fromDatePrice || !toDatePrice || fromDatePrice == 0) {
    return null
  }

  return toDatePrice / fromDatePrice;
};

const getPrice = async (time, instrumentId) => {
  let date = new Date(time * 1000);
  var tomorrow = new Date(date);
  tomorrow.setDate(date.getDate()+1);

  let data = await bitazzawsService.GetTickerHistory(instrumentId, 60, dateFormat(date, "yyyy-mm-dd"), dateFormat(date, "yyyy-mm-dd")); 
  
  if (!data[0]) {
    return 0;
  }

  let price = data[0][4]; // close

  for (let i = 0; i < data.length; i++) {
    if (data[i][0] > time * 1000) {
      break;
    }
    price = data[i][4];

  }

  return price;
}



module.exports = {
  getGainers,
  getlLosers,
};
