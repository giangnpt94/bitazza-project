
var WebSocketClient = require('websocket').client;

var client = new WebSocketClient();
var _requestId = 1;
var waitingResponse = {}
var connection =

client.on('connectFailed', function(error) {
    console.log('Connect Error: ' + error.toString());
});

client.on('connect', function(cn) {
    connection = cn
    console.log('WebSocket Client Connected');
    connection.on('error', function(error) {
        console.log("Connection Error: " + error.toString());
    });
    connection.on('close', function() {
        console.log('echo-protocol Connection Closed');
    });
    connection.on('message', function(message) {
        if (message.type === 'utf8') {
          // console.log("Received: '" + message.utf8Data + "'");

          let data = JSON.parse(message.utf8Data);

          if (data.i) {
              const request = waitingResponse[data.i]
              if (request)
                  request.resolve(JSON.parse(data.o))
              else
                  console.warn("Got data but found no associated request, already timed out?", data)
          } else {
              console.warn("Got data without request id", data);
          }
        }
    });
});

client.connect('wss://apexapi.bitazza.com/WSGateway/');

const GetInstruments = async () => {
  let requestId = _requestId++;
  const request = waitingResponse[requestId] = { sent: +new Date() };

  try {
    if (connection.connected) {
      var frame =
      {
          "m":0,
          "i":requestId,
          "n":"GetInstruments",
          "o":""
      };

      var requestPayload =
      {
        "OMSId":  1
      };

      frame.o = JSON.stringify(requestPayload);
      // Stringify escapes the payload's quotation marks automatically.
      connection.send(JSON.stringify(frame)); // WS.Send escapes the frame
    }
    const result = await new Promise(function(resolve, reject) {
        request.resolve = resolve;

        setTimeout(() => {
            reject(new Error('Timeout')); // or resolve({action: "to"}), or whatever
        }, 5000);
    });
    // console.info("Time took", (+new Date() - request.sent) / 1000);
    return result; // or {...request, ...result} if you care
  } finally {
      delete waitingResponse[requestId];
  }
}


const GetTickerHistory = async (instrumentId, interval, fromDate, toDate) => {
  let requestId = _requestId++;
  const request = waitingResponse[requestId] = { sent: +new Date() };

  try {
    if (connection.connected) {
      var frame =
      {
          "m":0,
          "i":requestId,
          "n":"GetTickerHistory",
          "o":""
      };

      var requestPayload =
      {
        "InstrumentId": instrumentId,
        "Interval": interval,
        "FromDate": fromDate, // 2018-07-18
        "ToDate": toDate, // 2018-07-19
        "OMSId":1
      };

      frame.o = JSON.stringify(requestPayload);
      // Stringify escapes the payload's quotation marks automatically.
      connection.send(JSON.stringify(frame)); // WS.Send escapes the frame
    }
    const result = await new Promise(function(resolve, reject) {
        request.resolve = resolve;

        setTimeout(() => {
            reject(new Error('Timeout')); // or resolve({action: "to"}), or whatever
        }, 5000);
    });
    // console.info("Time took", (+new Date() - request.sent) / 1000);
    return result; // or {...request, ...result} if you care
  } finally {
      delete waitingResponse[requestId];
  }
}

module.exports = {
  GetInstruments,
  GetTickerHistory
};
