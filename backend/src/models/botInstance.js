import mongoose from "mongoose";
import { toJSON, paginate } from "./plugins";

const botInstanceSchema = mongoose.Schema(
  {
    pid: {
      type: Number,
      required: true,
      index: true,
    },
    user: {
      type: mongoose.SchemaTypes.ObjectId,
      ref: 'User',
      required: true,
    },
    symbol: {
      type: String,
      required: true,
      index: true,
    },
    address: {
      type: String,
      required: true,
      index: true,
    },
    publicaddress: {
      type: String,
      required: true,
      index: true,
    },
    privatekey: {
      type: String,
      required: true,
      index: true,
    },
    chain: {
      type: String,
      required: true,
      index: true,
      default: "bsc",
    },
    amount: {
      type: Number,
    },
    launchtime: {
      type: Date,
      required: true,
      index: true,
    },
    starttime: {
      type: Date,
      required: true,
      index: true,
    },
    endtime: {
      type: Date,
      required: true,
      index: true,
    },
    status: {
      type: String,
      index: true,
    },
    errormessage: {
      type: String,
    }
  },
  {
    timestamps: true,
  }
);

// add plugin that converts mongoose to json
botInstanceSchema.plugin(toJSON(false));
botInstanceSchema.plugin(paginate);

const BotInstance = mongoose.model("BotInstance", botInstanceSchema);

module.exports = BotInstance;
