import paginate from "./paginate";
import toJSON from "./toJSON";

module.exports = {
  paginate,
  toJSON,
};
