import * as AuthController from "./auth";
import * as ApiController from "./api";

module.exports = {
  AuthController,
  ApiController,
};
