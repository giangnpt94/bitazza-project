import httpStatus from "http-status";
import catchAsync from "../utils/catchAsync";
import ApiError from "../utils/ApiError";
import { ApiService } from "../services";

export const getGainers = catchAsync(async (req, res) => {
  const result = await ApiService.getGainers(req.query.fromTime, req.query.toTime);
  res.send(result);
});

export const getlLosers = catchAsync(async (req, res) => {
  const result = await ApiService.getlLosers(req.query.fromTime, req.query.toTime);
  res.send(result);
});