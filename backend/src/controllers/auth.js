import httpStatus from "http-status";
import catchAsync from "../utils/catchAsync";
import ApiError from "../utils/ApiError";
import { AuthService, TokenService } from "../services";

export const login = catchAsync(async (req, res) => {
  const { email, password } = req.body;
  const user = await AuthService.loginUserWithEmailAndPassword(email, password);
  const { access } = await TokenService.generateAuthTokens(user);
  res.send({ userInfo: user, token: access.token });
});

export const logout = catchAsync(async (req, res) => {
  await AuthService.logout(req.body.refreshToken);
  res.status(httpStatus.NO_CONTENT).send();
});

export const getProfile = catchAsync(async (req, res) => {
  try {
    if (!req.user) {
      throw new Error();
    }
    res.send({ userInfo: req.user });
  } catch (error) {
    throw new ApiError(httpStatus.UNAUTHORIZED, "Please authenticate");
  }
});
