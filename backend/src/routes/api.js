import express from "express";
import { ApiController } from "../controllers";
import auth from '../middlewares/auth';

const router = express.Router();

router.get("/gainers"/*, auth()*/, ApiController.getGainers)
router.get("/losers"/*, auth()*/, ApiController.getlLosers)

module.exports = router;
