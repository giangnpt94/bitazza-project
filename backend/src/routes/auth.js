import express from"express";
import { AuthController } from "../controllers";
import auth from '../middlewares/auth';

const router = express.Router();

router.post("/login", AuthController.login);
router.post("/logout", AuthController.logout);
router.get("/profile", auth(), AuthController.getProfile)

module.exports = router;
