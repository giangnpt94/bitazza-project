import http from "http";
import createError from "http-errors";
import express from "express";
import path from "path";
import morgan from "morgan";
import cors from "cors";

import passport from "passport";
import winston from "./config/winston";

import AppConf from "./config/application";
import Messages from "./config/messages";

// setting router
import apiRouter from "./routes/api";
import authRouter from "./routes/auth";

import ws  from "./wserver";

const https = require('https');
const fs = require('fs');

const app = express();

app.use(morgan("combined", { stream: winston.stream }));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, "public")));
const { jwtStrategy } = require("./config/passport");

// set header
app.disable("x-powered-by");
app.use(
  cors({
    allowedOrigin: AppConf.cors.allowedOrigin,
    allowedMethods: AppConf.cors.allowedMethods,
    allowedHeaders: AppConf.cors.allowedHeaders,
    exposedHeaders: AppConf.cors.exposedHeaders,
    credentials: AppConf.cors.credentials,
    origin: true
  })
);

app.use(passport.initialize());
passport.use("jwt", jwtStrategy);

// setting Router
app.use("/api/v1", apiRouter);
app.use("/api/auth", authRouter);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get("env") === "development" ? err : {};

  // add this line to include winston logging
  winston.error(
    `${err.status || 500} - ${req.method} - ${err.message} - ${req.originalUrl
    } - ${req.ip} - ${req.user && req.user.email}`
  );
  console.log(
    `${err.status || 500} - ${req.method} - ${err.message} - ${req.originalUrl
    } - ${req.ip} - ${req.user && req.user.email}`
  );
  // render the error page
  res.status(err.status || err.statusCode || 500);
  res.json(
    !err.status && !err.statusCode
      ? { error: Messages["jp"].unexpected_error }
      : { error: err.message }
  );
});

/**
 * Get port from environment and store in Express.
 */
const port = normalizePort(process.env.PORT || "8082");
const httpsPort = normalizePort(process.env.HTTPS_PORT || "8443");
app.set("port", port);

/**
 * Create HTTP server.
 */
// const privateKey = fs.readFileSync('sslcert/server.key', 'utf8');
// const certificate = fs.readFileSync('sslcert/server.crt', 'utf8');
// const credentials = { key: privateKey, cert: certificate };
const server = http.createServer(app);
// const httpsServer = https.createServer(credentials, app);
server.on("error", onError);
server.on("listening", onListening);
// httpsServer.on("error", onErrorHttps);
// httpsServer.on("listening", onListeningHttps);

server.listen(port);
/**
 * Normalize a port into a number, string, or false.
 *
 * @param {*} val port
 */
function normalizePort(val) {
  const port = parseInt(val, 10);

  if (isNaN(port)) {
    // named pipe
    return val;
  }

  if (port >= 0) {
    // port number
    return port;
  }

  return false;
}

/**
 * Event listener for HTTP server "error" event.
 *
 * @param {*} error
 */
function onError(error) {
  if (error.syscall !== "listen") {
    throw error;
  }

  const bind = typeof port === "string" ? "Pipe " + port : "Port " + port;

  // handle specific listen errors with friendly messages
  switch (error.code) {
    case "EACCES":
      console.error(Messages["jp"].invalid_permission(bind));
      process.exit(1);
    case "EADDRINUSE":
      console.error(Messages["jp"].port_in_use(bind));
      process.exit(1);
    default:
      throw error;
  }
}

/**
 * Event listener for HTTP server "listening" event.
 */
function onListening() {
  const addr = server.address();
  const bind = typeof addr === "string" ? "pipe " + addr : "port " + addr.port;
  console.log(Messages["jp"].started_app(bind));
}


/**
 * Event listener for HTTPS server "error" event.
 *
 * @param {*} error
 */
function onErrorHttps(error) {
  if (error.syscall !== "listen") {
    throw error;
  }

  const bind = typeof httpsPort === "string" ? "Pipe " + httpsPort : "Port " + httpsPort;

  // handle specific listen errors with friendly messages
  switch (error.code) {
    case "EACCES":
      console.error(Messages["jp"].invalid_permission(bind));
      process.exit(1);
    case "EADDRINUSE":
      console.error(Messages["jp"].port_in_use(bind));
      process.exit(1);
    default:
      throw error;
  }
}

/**
 * Event listener for HTTPS server "listening" event.
 */
function onListeningHttps() {
  const addr = server.address();
  const bind = typeof addr === "string" ? "pipe " + addr : "port " + httpsPort;
  console.log(Messages["jp"].started_app(bind));
}

module.exports = server;
