// Node.js WebSocket server script
const http = require('http');
const WebSocketServer = require('websocket').server;
const server = http.createServer();
server.listen(8081);
const wsServer = new WebSocketServer({
    httpServer: server
});
wsServer.on('request', function (request) {
    console.log('Client has connected.');
    const connection = request.accept(null, request.origin);
    connection.on('message', function (message) {
        console.log('Received Message:', message.utf8Data);
        let data = JSON.parse(message.utf8Data);
        let functinName = data.n;
        console.log(functinName);
        if (functinName == 'AuthenticateUser') {
            let response = {
                m: 1,
                i: 0,
                n: "AuthenticateUser",
                o: {
                    authenticated: true,
                    user:
                    {
                        userId: 1,
                        userName: "namdq",
                        email: "namquoc.dev@gmail.com",
                        emailVerified: false,
                        accountId: 1,
                        omsId: 1,
                        use2FA: false
                    },
                    locked: false,
                    requires2FA: false,
                    twoFAType: "Google",
                    twoFAToken: "twoFAType",
                    errormsg: "Login success"
                }
            }
            let responseFail = {
                m: 1,
                i: 0,
                n: "AuthenticateUser",
                o: {
                    authenticated: false,

                    errormsg: "Login fail"
                }
            }
            console.log('Response Message:', JSON.stringify(responseFail));
            connection.sendUTF(JSON.stringify(response));
        }

        if (functinName == 'LogOut') {
            let response = {
                m: 1,
                i: 0,
                n: "LogOut",
                o: {
                    "result": true,
                    "errormsg": "Log out success",
                    "errorcode": 0,
                    "detail": null
                }
            }
            let responseFail = {
                m: 1,
                i: 0,
                n: "LogOut",
                o: {
                    "result": false,
                    "errormsg": "Log out fail",
                    "errorcode": 0,
                    "detail": null
                }
            }
            console.log('Response Message:', JSON.stringify(response));
            connection.sendUTF(JSON.stringify(response));
        }

    });
    connection.on('close', function (reasonCode, description) {
        console.log('Client has disconnected.');
    });
});
console.log('web socket running on port 8081');