const roles = ["user", "root", "admin"];

const roleRights = new Map();
roleRights.set(roles[0], []);
roleRights.set(roles[1], ["all"]);
roleRights.set(roles[2], ["getUsers", "manageUsers", "manageConfigs", "manageEth2"]);

module.exports = {
  roles,
  roleRights,
};
