export default {
  'en': {},
  'jp': {
    // System messages
    port_in_use: bind => `${bind}ポートは既に使用されています。`,
    invalid_permission: bind => `${bind}ポートは管理者権限が必要です。`,
    started_app: bind => `${bind}ポートで開始しました。`,
    connection_success_sequelize: 'データベース接続は正常に確立されました。',
    connection_failed_sequelize: error => `データベースに接続できない：${error}`,
    unexpected_error: 'システムエラーが発生しました。',
    // Authentication messages
    access_denied: '許可されていないページです。',
    login_error: [
      { type: 0, message: 'ログインアカウントの登録がありません。' },
      { type: 1, message: 'ログインに失敗しました。' }
    ],
    open_id_error: err => `OpenId error: ${err}`,
    // Domain management messages
    invalid_validate_input: '入力内容が不正です。',
    found_more_than_1_user: '複数のユーザーが見つかりました。',
    user_wrong_name_or_password: '名前またはパスワードが登録されているデータと異なります。',
    found_more_than_1_bill: '複数の請求情報が見つかりました。',
    wrong_bill_information: '請求情報が登録されているデータと異なります。',
    domain_not_found: '組織が見つかりませんでした。',
    domain_missing_user: '組織にユーザーの登録がありません。',
    wrong_password: 'パスワードが登録されたデータと異なります。',
    wrong_user_info: 'ログインID、パスワード、またはDirectログインIDが登録されていたデータと異なります。',
    wrong_user_name: 'アカウント名が登録されたデータと異なります。',
    bill_not_found: '請求情報が見つかりませんでした。',
    wrong_loginId: 'ログインIDが登録されたデータと異なります。',
    wrong_directLoginId: 'directログインIDが登録されたデータと異なります。',
    from_date_gt_to_date: 'トライアル期間の指定が不正です。'
  }
}
