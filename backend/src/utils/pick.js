/**
 * Create an object composed of the picked object properties
 * @param {Object} object
 * @param {string[]} keys
 * @returns {Object}
 */
const pick = (object, keys) => {
  return keys.reduce((obj, key) => {
    
    let keyOrginal = key;

    if (key.indexOf(":") >= 0) {
      let keys = key.split(":")
      key = keys[0]
    }
    
    if (object && Object.prototype.hasOwnProperty.call(object, key)) {
      // eslint-disable-next-line no-param-reassign
      if (object[key]) {
        if (keyOrginal.indexOf(":") >= 0) {
          let keys = keyOrginal.split(":")
          obj[key] = {}
          if (keys[1] === "$in") {
            obj[key][keys[1]] = object[key].split(",")
          } else {
              obj[key][keys[1]] = object[key]
          }
        } else {
          obj[key] = object[key];
        }
      }
    }
    return obj;
  }, {});
};

module.exports = pick;
